package com.example.bellido.ejercicio25

import android.content.DialogInterface
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), View.OnClickListener{

    val listaNumeros = mutableListOf<Int>()
    var num1 : Int = 0
    var num2 : Int = 0
    var resultado : Int = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        boton0.setOnClickListener(this)
        boton1.setOnClickListener(this)
        boton2.setOnClickListener(this)
        boton3.setOnClickListener(this)
        boton4.setOnClickListener(this)
        boton5.setOnClickListener(this)
        boton6.setOnClickListener(this)
        boton7.setOnClickListener(this)
        boton8.setOnClickListener(this)
        boton9.setOnClickListener(this)
        botonSuma.setOnClickListener(this)
        botonBorrar.setOnClickListener(this)
        botonIgual.setOnClickListener(this)


    }

    override fun onClick(v: View?) {

        when(v) {
            boton1 ->  resultat.setText(resultat.text.toString().plus(1))
            boton2 ->  resultat.setText(resultat.text.toString().plus(2))
            boton3 ->  resultat.setText(resultat.text.toString().plus(3))
            boton4 ->  resultat.setText(resultat.text.toString().plus(4))
            boton5 ->  resultat.setText(resultat.text.toString().plus(5))
            boton6 ->  resultat.setText(resultat.text.toString().plus(6))
            boton7 ->  resultat.setText(resultat.text.toString().plus(7))
            boton8 ->  resultat.setText(resultat.text.toString().plus(8))
            boton9 ->  resultat.setText(resultat.text.toString().plus(9))
            boton0 -> if (resultat.text.toString() != ""){resultat.setText(resultat.text.toString().plus(0))}
            botonSuma -> {
                if (resultat.text.toString() == "") {
                    listaNumeros.add(0)
                }else {
                    listaNumeros.add(resultat.text.toString().toInt())
                    resultat.setText("")
                }
            }
            botonIgual -> {
                if (resultat.text.toString() == "") {
                    listaNumeros.add(0)
                }else {
                    listaNumeros.add(resultat.text.toString().toInt())
                    resultat.setText("")
                }
                for (i in listaNumeros){
                resultado += i

                }
                resultat.setText(resultado.toString())
                listaNumeros.clear()
                resultado=0;
            }

            botonBorrar -> {
                listaNumeros.clear()
                resultat.setText("")
            }
        }

        }



    }





